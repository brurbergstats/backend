package utils

import (
	"bytes"
	"encoding/json"
	"io"

	"gitlab.com/brurberg/log"
)

type LogPackage struct {
	Domain string  `json:"domain"`
	Key    string  `json:"key"`
	Data   log.Out `json:"data"`
}

func LogToPackage(l log.Out, domain, key string) (buf io.ReadWriter, err error) {
	buf = new(bytes.Buffer)
	err = json.NewEncoder(buf).Encode(LogPackage{Domain: domain, Key: key, Data: l})
	return
}
