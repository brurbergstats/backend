package helper

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/lib/pq"

	"gitlab.com/brurberg/log/v2"
	"gitlab.com/brurberglogs/backend/utils"
)

type DBConnect struct {
	DB_HOST     string
	DB_USER     string
	DB_PASSWORD string
	DB_NAME     string
}

type DB struct {
	Conn *sql.DB
}

func (dbc DBConnect) ConnectToDB() (*sql.DB, error) {
	dbinfo := ""
	if dbc.DB_PASSWORD == "" {
		dbinfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_NAME)
	} else {
		dbinfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_NAME)
	}

	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		return nil, err
	}

	return db, db.Ping()
}

func (db DB) NewDailyLogins(packet utils.LogPackage) (err error) {
	if packet.Domain == "" || packet.Data.Message == "" {
		log.Println("Log input error")
		return
	}
	result, err := db.Conn.Exec("INSERT INTO entries (domain, level, message, errorTime) VALUES ($1, $2, $3, $4);", packet.Domain, packet.Data.Level, packet.Data.Message, time.Now())
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}

type DailyLogin struct {
	Day    time.Time `json:"day"`
	Logins int       `json:"logins"`
}

func (db DB) GetDailyLogins() (dailyLogins []DailyLogin, err error) {
	//rows, err := db.Conn.Query("SELECT date_trunc('day', errortime) as Day, count(*) as SuccessfullLogins FROM entries WHERE context = 'Login' AND level = 3 GROUP BY Day ORDER BY Day;")
	rows, err := db.Conn.Query("SELECT day, logins FROM dailyLogins ORDER BY Day;")
	if err != nil {
		return
	}
	for rows.Next() {
		var dailyLogin DailyLogin
		if log.CheckError("Fetching Sites", rows.Scan(&dailyLogin.Day, &dailyLogin.Logins), log.Warning) {
			continue
		}
		dailyLogins = append(dailyLogins, dailyLogin)
	}
	return
}
