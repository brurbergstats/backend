set -e
if (( $# < 1 ))
then
    printf "%b" "Error. Not enough arguments.\n" >&2
    printf "%b" "usage: build.sh <version> [<message>]\n" >&2
    exit 1
elif (( $# > 2 ))
then
    printf "%b" "Error. Too many arguments.\n" >&2
    printf "%b" "usage: build.sh <version> [<message>]\n" >&2
    exit 2
fi

docker build -t dethsanius/brurberglogs_backend:$1 .
docker push dethsanius/brurberglogs_backend:$1

if (( $# == 2 ))
then
  git add -A
  git commit -am "$2"
  git tag v$1
  git push
  git push --tags
fi

nvim ../kubernetes/backend/deployment.yaml
kubectl apply -f ../kubernetes/backend/deployment.yaml
