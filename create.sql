DROP DATABASE IF EXISTS brurbergstats;
CREATE DATABASE brurbergstats;
\c brurbergstats;

CREATE TABLE DailyLogins (
  Id SERIAL PRIMARY KEY,
  Day TIMESTAMPTZ NOT NULL,
  Logins int NOT NULL,
  ConsentedAt TIMESTAMPTZ DEFAULT Now()
);
