package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/brurberg/log/v2"
	"gitlab.com/brurbergauth/backend/lib/jwt"
	req "gitlab.com/brurbergauth/backend/lib/req/gin"
	"gitlab.com/brurberglogs/backend/helper"
	"gitlab.com/brurberglogs/backend/utils"
)

func loadEnvVar() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
}

type DB struct {
	DB *helper.DB
}

func main() {
	loadEnvVar()

	r := gin.Default()
	dbCon, dbErr := helper.DBConnect{DB_HOST: os.Getenv("POSTGRES_HOST"), DB_USER: os.Getenv("POSTGRES_USER"), DB_PASSWORD: os.Getenv("POSTGRES_PASSWORD"), DB_NAME: os.Getenv("POSTGRES_DB")}.ConnectToDB()
	if dbErr != nil {
		log.Fatal(dbErr)
	}
	db := DB{DB: &helper.DB{Conn: dbCon}}
	/*
		login := login.DB{DB: db.DB}
		reg := register.DB{DB: db.DB}
		upd := update.DB{DB: db.DB}
	*/

	api := r.Group("/api")
	auth := api.Group("/")
	auth.Use(AuthMiddleware())
	auth.GET("/dailylogins", db.DailyLogins)

	srv := api.Group("/")
	srv.Use(SrvMiddleware())
	srv.POST("/newdailylogin", func(c *gin.Context) {
		var query utils.LogPackage
		c.BindJSON(&query)
		err := db.DB.NewDailyLogins(query)
		if err != nil {
			log.Println(err)
		}
	})

	r.Run(":3000")
}

func (db DB) DailyLogins(c *gin.Context) {
	dailyLogins, err := db.DB.GetDailyLogins()
	if log.CheckError("Current Status", err, log.Error) {
		c.JSON(500, gin.H{"error": err.Error()})
		return
	}

	c.JSON(200, dailyLogins)
}

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		bearerToken, err := req.GetBearerToken(c.Request.Header.Get("authorization"))
		if log.CheckError("AuthMiddleware", err, log.Warning) {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Missmatch in hosts"})
			c.Abort()
			return
		}
		claims := jwt.GetClaims(bearerToken)
		if c.Keys == nil {
			c.Keys = make(map[string]interface{})
		}
		c.Keys["claims"] = claims
		if h, ok := claims["host"]; !ok || c.Request.Host != h {
			c.JSON(http.StatusForbidden, gin.H{"error": "Missmatch in hosts"})
			c.Abort()
			return
		}
		_, err = jwt.Verify(bearerToken, []byte(os.Getenv("JWT_SECRET")))
		if log.CheckError("AuthMiddleware", err, log.Warning) {
			c.JSON(http.StatusForbidden, gin.H{"error": "Missmatch in hosts"})
			c.Abort()
			return
		}
		c.Next()
	}
}

func SrvMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.Request.Header.Get("authorization") != os.Getenv("SRV_SECRET") {
			c.JSON(http.StatusForbidden, gin.H{"error": "Unauthrozed"})
			c.Abort()
			return
		}
		c.Next()
	}
}
